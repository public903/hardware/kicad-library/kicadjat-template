EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title "2001 RD Ondrej Kiš"
Date "2020-03-14"
Rev "1"
Comp "Ján Trnik"
Comment1 "Domový rozvádzač RD1"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 750  750  0    50   ~ 0
L1
Text Label 750  850  0    50   ~ 0
L2
Text Label 750  950  0    50   ~ 0
L3
Text Label 750  1050 0    50   ~ 0
N
Text Notes 11000 950  0    50   ~ 0
/3
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-4p-jt_elektro W?
U 1 1 5E6DE5D9
P 1400 5800
AR Path="/5E6DE5D9" Ref="W?"  Part="1" 
AR Path="/5E6D252B/5E6DE5D9" Ref="W8"  Part="1" 
F 0 "W8" H 1400 6150 50  0000 C CNN
F 1 "Kablovy_vyvod-4p" H 1400 6150 50  0001 C CNN
F 2 "" H 1400 5900 50  0001 C CNN
F 3 "" H 1400 5900 50  0001 C CNN
F 4 "AYKY-J" H 1400 6050 50  0000 C CNN "Popis1"
F 5 "4x16" H 1400 5950 50  0000 C CNN "Popis2"
	1    1400 5800
	1    0    0    -1  
$EndComp
Text Label 750  5000 0    50   ~ 0
PE
$Comp
L power:Earth #PWR?
U 1 1 5E6DE5E0
P 750 5000
AR Path="/5E6DE5E0" Ref="#PWR?"  Part="1" 
AR Path="/5E6D252B/5E6DE5E0" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 750 4750 50  0001 C CNN
F 1 "Earth" H 750 4850 50  0001 C CNN
F 2 "" H 750 5000 50  0001 C CNN
F 3 "~" H 750 5000 50  0001 C CNN
	1    750  5000
	1    0    0    -1  
$EndComp
Text Notes 1150 5900 0    50   ~ 0
Prívod z RD2
Wire Wire Line
	1600 5200 1600 5000
Connection ~ 1600 5000
Wire Wire Line
	750  1050 950  1050
Wire Wire Line
	750  5000 950  5000
Wire Wire Line
	750  850  1350 850 
$Comp
L dom-ondrik-kovacova-rescue:Vypinac-3f-jt_elektro QM1
U 1 1 5E6E1A64
P 1200 3300
F 0 "QM1" H 1550 4050 50  0000 L CNN
F 1 "Hl.vyp." H 1550 3950 50  0000 L CNN
F 2 "" H 1200 3400 50  0001 C CNN
F 3 "" H 1200 3400 50  0001 C CNN
	1    1200 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 5200 1200 3300
Wire Wire Line
	1300 5200 1300 3300
Wire Wire Line
	1300 3300 1350 3300
Wire Wire Line
	1400 5200 1400 3400
Wire Wire Line
	1350 2500 1350 850 
Connection ~ 1350 850 
Wire Wire Line
	950  5000 950  1050
Connection ~ 950  5000
Wire Wire Line
	950  5000 1600 5000
Connection ~ 950  1050
Wire Wire Line
	950  1050 2300 1050
Wire Wire Line
	1400 3400 1500 3400
Wire Wire Line
	1500 3400 1500 3300
Wire Wire Line
	750  750  1200 750 
Wire Wire Line
	750  950  1500 950 
Wire Wire Line
	1200 2500 1200 750 
Connection ~ 1200 750 
Wire Wire Line
	1200 750  2000 750 
Wire Wire Line
	1500 2500 1500 950 
Connection ~ 1500 950 
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA2
U 1 1 5E6EC906
P 2000 3300
AR Path="/5E6EC906" Ref="FA2"  Part="1" 
AR Path="/5E6D252B/5E6EC906" Ref="FA2"  Part="1" 
F 0 "FA2" H 2050 4050 50  0000 L CNN
F 1 "B10A" H 2050 3950 50  0000 L CNN
F 2 "" H 2000 3400 50  0001 C CNN
F 3 "" H 2000 3400 50  0001 C CNN
	1    2000 3300
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA3
U 1 1 5E6EDB90
P 2550 3300
AR Path="/5E6EDB90" Ref="FA3"  Part="1" 
AR Path="/5E6D252B/5E6EDB90" Ref="FA3"  Part="1" 
F 0 "FA3" H 2600 4050 50  0000 L CNN
F 1 "B10A" H 2600 3950 50  0000 L CNN
F 2 "" H 2550 3400 50  0001 C CNN
F 3 "" H 2550 3400 50  0001 C CNN
	1    2550 3300
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA4
U 1 1 5E6EDCE4
P 3100 3300
AR Path="/5E6EDCE4" Ref="FA4"  Part="1" 
AR Path="/5E6D252B/5E6EDCE4" Ref="FA4"  Part="1" 
F 0 "FA4" H 3150 4050 50  0000 L CNN
F 1 "B10A" H 3150 3950 50  0000 L CNN
F 2 "" H 3100 3400 50  0001 C CNN
F 3 "" H 3100 3400 50  0001 C CNN
	1    3100 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2500 2000 750 
Wire Wire Line
	2550 2500 2550 850 
Wire Wire Line
	3100 2500 3100 950 
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W9
U 1 1 5E6F15B2
P 2200 5800
F 0 "W9" H 2200 6150 50  0000 C CNN
F 1 "Kablovy_vyvod-3p" H 2200 6150 50  0001 C CNN
F 2 "" H 2200 5900 50  0001 C CNN
F 3 "" H 2200 5900 50  0001 C CNN
F 4 "CYKY-J" H 2200 6050 50  0000 C CNN "Popis1"
F 5 "3x1.5" H 2200 5950 50  0000 C CNN "Popis2"
	1    2200 5800
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W10
U 1 1 5E6F29FA
P 2750 5800
F 0 "W10" H 2750 6150 50  0000 C CNN
F 1 "Kablovy_vyvod-3p" H 2750 6150 50  0001 C CNN
F 2 "" H 2750 5900 50  0001 C CNN
F 3 "" H 2750 5900 50  0001 C CNN
F 4 "CYKY-J" H 2750 6050 50  0000 C CNN "Popis1"
F 5 "3x1.5" H 2750 5950 50  0000 C CNN "Popis2"
	1    2750 5800
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W11
U 1 1 5E6F31AF
P 3300 5800
F 0 "W11" H 3300 6150 50  0000 C CNN
F 1 "Kablovy_vyvod-3p" H 3300 6150 50  0001 C CNN
F 2 "" H 3300 5900 50  0001 C CNN
F 3 "" H 3300 5900 50  0001 C CNN
F 4 "CYKY-J" H 3300 6050 50  0000 C CNN "Popis1"
F 5 "3x1.5" H 3300 5950 50  0000 C CNN "Popis2"
	1    3300 5800
	1    0    0    -1  
$EndComp
Connection ~ 3100 950 
Wire Wire Line
	3100 950  4750 950 
Wire Wire Line
	1350 850  2550 850 
Wire Wire Line
	1500 950  3100 950 
Wire Wire Line
	2000 5200 2000 3300
Wire Wire Line
	2550 5200 2550 3300
Wire Wire Line
	3100 5200 3100 3300
Connection ~ 2550 850 
Wire Wire Line
	2550 850  4200 850 
Connection ~ 2000 750 
Wire Wire Line
	2000 750  3650 750 
Wire Wire Line
	2300 5200 2300 1050
Connection ~ 2300 1050
Wire Wire Line
	2300 1050 2850 1050
Wire Wire Line
	2850 5200 2850 1050
Connection ~ 2850 1050
Wire Wire Line
	2850 1050 3400 1050
Wire Wire Line
	3400 5200 3400 1050
Connection ~ 3400 1050
Wire Wire Line
	3400 1050 3950 1050
Wire Wire Line
	3500 5200 3500 5000
Connection ~ 3500 5000
Wire Wire Line
	3500 5000 4050 5000
Wire Wire Line
	2950 5200 2950 5000
Connection ~ 2950 5000
Wire Wire Line
	2950 5000 3500 5000
Wire Wire Line
	2400 5200 2400 5000
Wire Wire Line
	1600 5000 2400 5000
Connection ~ 2400 5000
Wire Wire Line
	2400 5000 2950 5000
Text Notes 2050 6050 0    50   ~ 0
Svetlo\ndetská\ndole
Text Notes 2550 6000 0    50   ~ 0
Svetlo pod \nschodami
Text Notes 3100 6050 0    50   ~ 0
Svetlo\n1x kumbál\n2x kuchyňa
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W12
U 1 1 5E6D4D11
P 3850 5800
F 0 "W12" H 3850 6150 50  0000 C CNN
F 1 "Kablovy_vyvod-3p" H 3850 6150 50  0001 C CNN
F 2 "" H 3850 5900 50  0001 C CNN
F 3 "" H 3850 5900 50  0001 C CNN
F 4 "CYKY-J" H 3850 6050 50  0000 C CNN "Popis1"
F 5 "3x1.5" H 3850 5950 50  0000 C CNN "Popis2"
	1    3850 5800
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W13
U 1 1 5E6D4D19
P 4400 5800
F 0 "W13" H 4400 6150 50  0000 C CNN
F 1 "Kablovy_vyvod-3p" H 4400 6150 50  0001 C CNN
F 2 "" H 4400 5900 50  0001 C CNN
F 3 "" H 4400 5900 50  0001 C CNN
F 4 "CYKY-J" H 4400 6050 50  0000 C CNN "Popis1"
F 5 "3x2.5" H 4400 5950 50  0000 C CNN "Popis2"
	1    4400 5800
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W14
U 1 1 5E6D4D21
P 4950 5800
F 0 "W14" H 4950 6150 50  0000 C CNN
F 1 "Kablovy_vyvod-3p" H 4950 6150 50  0001 C CNN
F 2 "" H 4950 5900 50  0001 C CNN
F 3 "" H 4950 5900 50  0001 C CNN
F 4 "CYKY-J" H 4950 6050 50  0000 C CNN "Popis1"
F 5 "3x2.5" H 4950 5950 50  0000 C CNN "Popis2"
	1    4950 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 5200 3650 3300
Wire Wire Line
	4200 5200 4200 3300
Wire Wire Line
	4750 5200 4750 3300
Wire Wire Line
	3950 5200 3950 1050
Wire Wire Line
	4500 5200 4500 1050
Wire Wire Line
	5050 5200 5050 1050
Wire Wire Line
	5150 5200 5150 5000
Wire Wire Line
	4600 5200 4600 5000
Wire Wire Line
	4050 5200 4050 5000
Text Notes 3600 6050 0    50   ~ 0
Svetlo\nchodba dole\nobývačka
Text Notes 4150 6000 0    50   ~ 0
Zásuvky\nobývačka 4x
Text Notes 4750 6050 0    50   ~ 0
Zásuvka\nkuchyňa\nľavá
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W15
U 1 1 5E6E008B
P 5500 5800
F 0 "W15" H 5500 6150 50  0000 C CNN
F 1 "Kablovy_vyvod-3p" H 5500 6150 50  0001 C CNN
F 2 "" H 5500 5900 50  0001 C CNN
F 3 "" H 5500 5900 50  0001 C CNN
F 4 "CYKY-J" H 5500 6050 50  0000 C CNN "Popis1"
F 5 "3x2.5" H 5500 5950 50  0000 C CNN "Popis2"
	1    5500 5800
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W16
U 1 1 5E6E0093
P 6050 5800
F 0 "W16" H 6050 6150 50  0000 C CNN
F 1 "Kablovy_vyvod-3p" H 6050 6150 50  0001 C CNN
F 2 "" H 6050 5900 50  0001 C CNN
F 3 "" H 6050 5900 50  0001 C CNN
F 4 "CYKY-J" H 6050 6050 50  0000 C CNN "Popis1"
F 5 "3x2.5" H 6050 5950 50  0000 C CNN "Popis2"
	1    6050 5800
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W17
U 1 1 5E6E009B
P 6600 5800
F 0 "W17" H 6600 6150 50  0000 C CNN
F 1 "Kablovy_vyvod-3p" H 6600 6150 50  0001 C CNN
F 2 "" H 6600 5900 50  0001 C CNN
F 3 "" H 6600 5900 50  0001 C CNN
F 4 "CYKY-J" H 6600 6050 50  0000 C CNN "Popis1"
F 5 "3x2.5" H 6600 5950 50  0000 C CNN "Popis2"
	1    6600 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 5200 5300 3300
Wire Wire Line
	5850 5200 5850 3300
Wire Wire Line
	6400 5200 6400 3300
Wire Wire Line
	5600 5200 5600 1050
Wire Wire Line
	6150 5200 6150 1050
Wire Wire Line
	6700 5200 6700 1050
Wire Wire Line
	6800 5200 6800 5000
Wire Wire Line
	6250 5200 6250 5000
Wire Wire Line
	5700 5200 5700 5000
Text Notes 5450 5900 0    50   ~ 0
?
Text Notes 5850 6000 0    50   ~ 0
Zásuvky:\nizba dole
Text Notes 6350 6050 0    50   ~ 0
Zásuvky hore\nspálňa\nbalkón 4x
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W18
U 1 1 5E6E00AF
P 7150 5800
F 0 "W18" H 7150 6150 50  0000 C CNN
F 1 "Kablovy_vyvod-3p" H 7150 6150 50  0001 C CNN
F 2 "" H 7150 5900 50  0001 C CNN
F 3 "" H 7150 5900 50  0001 C CNN
F 4 "CYKY-J" H 7150 6050 50  0000 C CNN "Popis1"
F 5 "3x2.5" H 7150 5950 50  0000 C CNN "Popis2"
	1    7150 5800
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W19
U 1 1 5E6E00B7
P 7700 5800
F 0 "W19" H 7700 6150 50  0000 C CNN
F 1 "Kablovy_vyvod-3p" H 7700 6150 50  0001 C CNN
F 2 "" H 7700 5900 50  0001 C CNN
F 3 "" H 7700 5900 50  0001 C CNN
F 4 "CYKY-J" H 7700 6050 50  0000 C CNN "Popis1"
F 5 "3x2.5" H 7700 5950 50  0000 C CNN "Popis2"
	1    7700 5800
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W21
U 1 1 5E6E00BF
P 8800 5800
F 0 "W21" H 8800 6150 50  0000 C CNN
F 1 "Kablovy_vyvod-3p" H 8800 6150 50  0001 C CNN
F 2 "" H 8800 5900 50  0001 C CNN
F 3 "" H 8800 5900 50  0001 C CNN
F 4 "CYKY-J" H 8800 6050 50  0000 C CNN "Popis1"
F 5 "3x2.5" H 8800 5950 50  0000 C CNN "Popis2"
	1    8800 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 5200 6950 3300
Wire Wire Line
	7500 5200 7500 3300
Wire Wire Line
	8050 5200 8050 5150
Wire Wire Line
	7250 5200 7250 1050
Wire Wire Line
	7800 5200 7800 1050
Wire Wire Line
	8350 5000 8350 1050
Wire Wire Line
	7900 5200 7900 5000
Wire Wire Line
	7350 5200 7350 5000
Text Notes 6900 6050 0    50   ~ 0
Zásuvky hore\nspálňa bez\nbalkónu 4x
Text Notes 7450 6150 0    50   ~ 0
Zásuvky\nkuchyňa\nstred+pravá+\n+kumbál
Text Notes 8100 6050 0    50   ~ 0
do RD2\nspínanie\nbojlera
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA5
U 1 1 5E6F33CA
P 3650 3300
AR Path="/5E6F33CA" Ref="FA5"  Part="1" 
AR Path="/5E6D252B/5E6F33CA" Ref="FA5"  Part="1" 
F 0 "FA5" H 3700 4050 50  0000 L CNN
F 1 "B6A" H 3700 3950 50  0000 L CNN
F 2 "" H 3650 3400 50  0001 C CNN
F 3 "" H 3650 3400 50  0001 C CNN
	1    3650 3300
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA6
U 1 1 5E6F33D0
P 4200 3300
AR Path="/5E6F33D0" Ref="FA6"  Part="1" 
AR Path="/5E6D252B/5E6F33D0" Ref="FA6"  Part="1" 
F 0 "FA6" H 4250 4050 50  0000 L CNN
F 1 "B16A" H 4250 3950 50  0000 L CNN
F 2 "" H 4200 3400 50  0001 C CNN
F 3 "" H 4200 3400 50  0001 C CNN
	1    4200 3300
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA7
U 1 1 5E6F33D6
P 4750 3300
AR Path="/5E6F33D6" Ref="FA7"  Part="1" 
AR Path="/5E6D252B/5E6F33D6" Ref="FA7"  Part="1" 
F 0 "FA7" H 4800 4050 50  0000 L CNN
F 1 "B16A" H 4800 3950 50  0000 L CNN
F 2 "" H 4750 3400 50  0001 C CNN
F 3 "" H 4750 3400 50  0001 C CNN
	1    4750 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2500 3650 750 
Wire Wire Line
	4200 2500 4200 850 
Wire Wire Line
	4750 2500 4750 950 
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA8
U 1 1 5E6F6768
P 5300 3300
AR Path="/5E6F6768" Ref="FA8"  Part="1" 
AR Path="/5E6D252B/5E6F6768" Ref="FA8"  Part="1" 
F 0 "FA8" H 5350 4050 50  0000 L CNN
F 1 "B16A" H 5350 3950 50  0000 L CNN
F 2 "" H 5300 3400 50  0001 C CNN
F 3 "" H 5300 3400 50  0001 C CNN
	1    5300 3300
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA9
U 1 1 5E6F676E
P 5850 3300
AR Path="/5E6F676E" Ref="FA9"  Part="1" 
AR Path="/5E6D252B/5E6F676E" Ref="FA9"  Part="1" 
F 0 "FA9" H 5900 4050 50  0000 L CNN
F 1 "B16A" H 5900 3950 50  0000 L CNN
F 2 "" H 5850 3400 50  0001 C CNN
F 3 "" H 5850 3400 50  0001 C CNN
	1    5850 3300
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA13
U 1 1 5E6F6774
P 6400 3300
AR Path="/5E6F6774" Ref="FA13"  Part="1" 
AR Path="/5E6D252B/5E6F6774" Ref="FA13"  Part="1" 
F 0 "FA13" H 6450 4050 50  0000 L CNN
F 1 "B16A" H 6450 3950 50  0000 L CNN
F 2 "" H 6400 3400 50  0001 C CNN
F 3 "" H 6400 3400 50  0001 C CNN
	1    6400 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2500 5300 750 
Wire Wire Line
	5850 2500 5850 850 
Wire Wire Line
	6400 2500 6400 950 
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA14
U 1 1 5E6F8E8F
P 6950 3300
AR Path="/5E6F8E8F" Ref="FA14"  Part="1" 
AR Path="/5E6D252B/5E6F8E8F" Ref="FA14"  Part="1" 
F 0 "FA14" H 7000 4050 50  0000 L CNN
F 1 "B16A" H 7000 3950 50  0000 L CNN
F 2 "" H 6950 3400 50  0001 C CNN
F 3 "" H 6950 3400 50  0001 C CNN
	1    6950 3300
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA15
U 1 1 5E6F8E95
P 7500 3300
AR Path="/5E6F8E95" Ref="FA15"  Part="1" 
AR Path="/5E6D252B/5E6F8E95" Ref="FA15"  Part="1" 
F 0 "FA15" H 7550 4050 50  0000 L CNN
F 1 "B16A" H 7550 3950 50  0000 L CNN
F 2 "" H 7500 3400 50  0001 C CNN
F 3 "" H 7500 3400 50  0001 C CNN
	1    7500 3300
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA16
U 1 1 5E6F8E9B
P 8050 3300
AR Path="/5E6F8E9B" Ref="FA16"  Part="1" 
AR Path="/5E6D252B/5E6F8E9B" Ref="FA16"  Part="1" 
F 0 "FA16" H 8100 4050 50  0000 L CNN
F 1 "B16A" H 8100 3950 50  0000 L CNN
F 2 "" H 8050 3400 50  0001 C CNN
F 3 "" H 8050 3400 50  0001 C CNN
	1    8050 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 2500 6950 750 
Wire Wire Line
	7500 2500 7500 850 
Wire Wire Line
	8050 2500 8050 950 
$Sheet
S 900  6800 500  300 
U 5E763809
F0 "FI1" 50
F1 "FI1.sch" 50
$EndSheet
Text Notes 1000 7000 0    98   ~ 20
FI1
Connection ~ 8350 1050
Wire Wire Line
	8350 1050 8900 1050
Connection ~ 8050 950 
Wire Wire Line
	8050 950  10850 950 
Connection ~ 7800 1050
Wire Wire Line
	7800 1050 8350 1050
Connection ~ 7500 850 
Wire Wire Line
	7500 850  10850 850 
Connection ~ 7250 1050
Wire Wire Line
	7250 1050 7800 1050
Connection ~ 6950 750 
Wire Wire Line
	6950 750  10850 750 
Connection ~ 6700 1050
Wire Wire Line
	6700 1050 7250 1050
Connection ~ 6400 950 
Wire Wire Line
	6400 950  8050 950 
Connection ~ 6150 1050
Wire Wire Line
	6150 1050 6700 1050
Connection ~ 5850 850 
Wire Wire Line
	5850 850  7500 850 
Connection ~ 5600 1050
Wire Wire Line
	5600 1050 6150 1050
Connection ~ 5300 750 
Wire Wire Line
	5300 750  6950 750 
Connection ~ 5050 1050
Wire Wire Line
	5050 1050 5600 1050
Connection ~ 4750 950 
Wire Wire Line
	4750 950  6400 950 
Connection ~ 4500 1050
Wire Wire Line
	4500 1050 5050 1050
Connection ~ 4200 850 
Wire Wire Line
	4200 850  5850 850 
Connection ~ 3950 1050
Wire Wire Line
	3950 1050 4500 1050
Connection ~ 3650 750 
Wire Wire Line
	3650 750  5300 750 
Connection ~ 4050 5000
Wire Wire Line
	4050 5000 4600 5000
Connection ~ 4600 5000
Wire Wire Line
	4600 5000 5150 5000
Connection ~ 5150 5000
Wire Wire Line
	5150 5000 5700 5000
Connection ~ 5700 5000
Wire Wire Line
	5700 5000 6250 5000
Connection ~ 6250 5000
Wire Wire Line
	6250 5000 6800 5000
Connection ~ 6800 5000
Wire Wire Line
	6800 5000 7350 5000
Connection ~ 7350 5000
Wire Wire Line
	7350 5000 7900 5000
Connection ~ 7900 5000
Wire Wire Line
	7900 5000 8350 5000
Connection ~ 8350 5000
Wire Wire Line
	8350 5000 9000 5000
Wire Wire Line
	8150 5200 8150 5150
Wire Wire Line
	8150 5150 8050 5150
Connection ~ 8050 5150
Wire Wire Line
	8050 5150 8050 3300
Wire Wire Line
	8250 5200 8250 5150
Wire Wire Line
	8250 5150 8450 5150
Wire Wire Line
	8600 5150 8600 5200
Wire Wire Line
	8450 5200 8450 5150
Connection ~ 8450 5150
Wire Wire Line
	8450 5150 8600 5150
Wire Wire Line
	8900 5200 8900 1050
Connection ~ 8900 1050
Wire Wire Line
	8900 1050 10850 1050
Wire Wire Line
	9000 5200 9000 5000
Connection ~ 9000 5000
Wire Wire Line
	9000 5000 10850 5000
Text Notes 8700 5900 0    50   ~ 0
bojler
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-4p-jt_elektro W20
U 1 1 5EB7DD00
P 8250 5800
F 0 "W20" H 8250 6150 50  0000 C CNN
F 1 "Kablovy_vyvod-4p" H 8250 6150 50  0001 C CNN
F 2 "" H 8250 5900 50  0001 C CNN
F 3 "" H 8250 5900 50  0001 C CNN
F 4 "CYKY-J" H 8250 6050 50  0000 C CNN "Popis1"
F 5 "4x1.5" H 8250 5950 50  0000 C CNN "Popis2"
	1    8250 5800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
