EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title "2001 RD Ondrej Kiš"
Date "2020-03-14"
Rev "1"
Comp "Ján Trnik"
Comment1 "Elektroinštalácia RD"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 6400 3250 500  300 
U 5E6D252B
F0 "R1-1" 50
F1 "R1-1.sch" 50
$EndSheet
Wire Notes Line style solid
	5300 3250 5300 3550
Wire Notes Line style solid
	5300 3550 5800 3550
Wire Notes Line style solid
	5800 3550 5800 3250
Wire Notes Line style solid
	5800 3250 5300 3250
Text Notes 6500 3450 0    98   ~ 20
RD1
Text Notes 5400 3450 0    98   ~ 20
RD2
Wire Notes Line style solid
	4200 3250 4200 3550
Wire Notes Line style solid
	4200 3550 4700 3550
Wire Notes Line style solid
	4700 3550 4700 3250
Wire Notes Line style solid
	4700 3250 4200 3250
Text Notes 4300 3450 0    98   ~ 20
RE
Wire Notes Line
	4700 3350 5300 3350
Wire Notes Line
	5800 3350 6400 3350
Text Notes 4750 3350 0    50   ~ 0
AYKY-J 4x16
Text Notes 5850 3350 0    50   ~ 0
AYKY-J 4x16
Wire Notes Line
	5800 3500 6400 3500
Text Notes 5850 3500 0    50   ~ 0
CYKY-J 4x1.5
Wire Notes Line
	4700 3500 5300 3500
Text Notes 4750 3500 0    50   ~ 0
CYKY-J 3x1.5
$EndSCHEMATC
