EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "2001 RD Ondrej Kiš"
Date "2020-03-14"
Rev "1"
Comp "Ján Trnik"
Comment1 "Domový rozvádzač RD1"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 750  750  0    50   ~ 0
L1
Text Label 750  850  0    50   ~ 0
L2
Text Label 750  950  0    50   ~ 0
L3
Text Label 750  1050 0    50   ~ 0
N
Text Label 750  5000 0    50   ~ 0
PE
$Comp
L power:Earth #PWR?
U 1 1 5E794176
P 750 5000
AR Path="/5E794176" Ref="#PWR?"  Part="1" 
AR Path="/5E6D252B/5E794176" Ref="#PWR?"  Part="1" 
AR Path="/5E6D252B/5E763809/5E794176" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 750 4750 50  0001 C CNN
F 1 "Earth" H 750 4850 50  0001 C CNN
F 2 "" H 750 5000 50  0001 C CNN
F 3 "~" H 750 5000 50  0001 C CNN
	1    750  5000
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W?
U 1 1 5E7941EC
P 3850 5800
AR Path="/5E6D252B/5E7941EC" Ref="W?"  Part="1" 
AR Path="/5E6D252B/5E763809/5E7941EC" Ref="W22"  Part="1" 
F 0 "W22" H 3800 6150 50  0000 L CNN
F 1 "Kablovy_vyvod-3p" H 3850 6150 50  0001 C CNN
F 2 "" H 3850 5900 50  0001 C CNN
F 3 "" H 3850 5900 50  0001 C CNN
F 4 "CYKY-J" H 3850 6050 50  0000 C CNN "Popis1"
F 5 "3x1.5" H 3850 5950 50  0000 C CNN "Popis2"
	1    3850 5800
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W?
U 1 1 5E7941F4
P 4400 5800
AR Path="/5E6D252B/5E7941F4" Ref="W?"  Part="1" 
AR Path="/5E6D252B/5E763809/5E7941F4" Ref="W23"  Part="1" 
F 0 "W23" H 4350 6150 50  0000 L CNN
F 1 "Kablovy_vyvod-3p" H 4400 6150 50  0001 C CNN
F 2 "" H 4400 5900 50  0001 C CNN
F 3 "" H 4400 5900 50  0001 C CNN
F 4 "CYKY-J" H 4400 6050 50  0000 C CNN "Popis1"
F 5 "3x2.5" H 4400 5950 50  0000 C CNN "Popis2"
	1    4400 5800
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Kablovy_vyvod-3p-jt_elektro W?
U 1 1 5E7941FC
P 4950 5800
AR Path="/5E6D252B/5E7941FC" Ref="W?"  Part="1" 
AR Path="/5E6D252B/5E763809/5E7941FC" Ref="W24"  Part="1" 
F 0 "W24" H 4900 6150 50  0000 L CNN
F 1 "Kablovy_vyvod-3p" H 4950 6150 50  0001 C CNN
F 2 "" H 4950 5900 50  0001 C CNN
F 3 "" H 4950 5900 50  0001 C CNN
F 4 "CYKY-J" H 4950 6050 50  0000 C CNN "Popis1"
F 5 "3x2.5" H 4950 5950 50  0000 C CNN "Popis2"
	1    4950 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 5200 4200 3300
Wire Wire Line
	4750 5200 4750 3300
Wire Wire Line
	5150 5200 5150 5000
Wire Wire Line
	4600 5200 4600 5000
Wire Wire Line
	4050 5200 4050 5000
Text Notes 3500 6400 0    50   ~ 0
SVETLO:\ncelý vrch\nschody\nZÁSUVKA: \npod schodami\nSVETLO:\nkúpelňa dole
Text Notes 4100 6150 0    50   ~ 0
ZÁSUVKY:\nchodba dole 1x\nkúpelňa dole 1x\ndetská izba 4x
Text Notes 4800 6250 0    50   ~ 0
ZÁSUVKY:\nchodba hore\nkúpelľa hore\nSVETLO:\nkúpelňa hore
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA?
U 1 1 5E79425C
P 4200 3300
AR Path="/5E6D252B/5E79425C" Ref="FA?"  Part="1" 
AR Path="/5E6D252B/5E763809/5E79425C" Ref="FA11"  Part="1" 
AR Path="/5E79425C" Ref="FA11"  Part="1" 
F 0 "FA11" H 4250 4050 50  0000 L CNN
F 1 "B16A" H 4250 3950 50  0000 L CNN
F 2 "" H 4200 3400 50  0001 C CNN
F 3 "" H 4200 3400 50  0001 C CNN
	1    4200 3300
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA?
U 1 1 5E794262
P 4750 3300
AR Path="/5E6D252B/5E794262" Ref="FA?"  Part="1" 
AR Path="/5E6D252B/5E763809/5E794262" Ref="FA12"  Part="1" 
AR Path="/5E794262" Ref="FA12"  Part="1" 
F 0 "FA12" H 4800 4050 50  0000 L CNN
F 1 "B16A" H 4800 3950 50  0000 L CNN
F 2 "" H 4750 3400 50  0001 C CNN
F 3 "" H 4750 3400 50  0001 C CNN
	1    4750 3300
	1    0    0    -1  
$EndComp
$Comp
L dom-ondrik-kovacova-rescue:Istic-1f-jt_elektro FA?
U 1 1 5E794256
P 3650 3300
AR Path="/5E6D252B/5E794256" Ref="FA?"  Part="1" 
AR Path="/5E6D252B/5E763809/5E794256" Ref="FA10"  Part="1" 
AR Path="/5E794256" Ref="FA10"  Part="1" 
F 0 "FA10" H 3700 4050 50  0000 L CNN
F 1 "B10A" H 3700 3950 50  0000 L CNN
F 2 "" H 3650 3400 50  0001 C CNN
F 3 "" H 3650 3400 50  0001 C CNN
	1    3650 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 5200 3650 3300
Wire Wire Line
	750  1050 1950 1050
Wire Wire Line
	750  750  1200 750 
Wire Wire Line
	750  850  1450 850 
$Comp
L dom-ondrik-kovacova-rescue:Prudovy_chranic-3f-jt_elektro FI1
U 1 1 5E7CB853
P 1200 2450
F 0 "FI1" H 750 3250 50  0000 L CNN
F 1 "25A/30mA" H 750 3150 50  0000 L CNN
F 2 "" H 1200 2550 50  0001 C CNN
F 3 "" H 1200 2550 50  0001 C CNN
	1    1200 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 1650 1200 750 
Connection ~ 1200 750 
Wire Wire Line
	1450 1650 1450 850 
Connection ~ 1450 850 
Wire Wire Line
	1700 1650 1700 950 
Wire Wire Line
	750  950  1700 950 
Connection ~ 1700 950 
Wire Wire Line
	1950 1650 1950 1050
Connection ~ 1950 1050
Wire Wire Line
	1450 850  10850 850 
Wire Wire Line
	1200 750  10850 750 
Wire Wire Line
	1950 1050 10850 1050
Wire Wire Line
	1700 950  10850 950 
Wire Wire Line
	2450 1650 4200 1650
Wire Wire Line
	2200 1550 3650 1550
Wire Wire Line
	2950 1850 3950 1850
Wire Wire Line
	2700 1750 4750 1750
Wire Wire Line
	1950 2450 1950 2550
Wire Wire Line
	1950 2550 2950 2550
Wire Wire Line
	2950 2550 2950 1850
Wire Wire Line
	2700 1750 2700 2650
Wire Wire Line
	2700 2650 1700 2650
Wire Wire Line
	1700 2650 1700 2450
Wire Wire Line
	1450 2450 1450 2750
Wire Wire Line
	1450 2750 2450 2750
Wire Wire Line
	2450 2750 2450 1650
Wire Wire Line
	1200 2450 1200 2850
Wire Wire Line
	1200 2850 2200 2850
Wire Wire Line
	2200 2850 2200 1550
Wire Wire Line
	3650 2500 3650 1550
Connection ~ 3650 1550
Wire Wire Line
	3650 1550 10500 1550
Wire Wire Line
	4200 2500 4200 1650
Connection ~ 4200 1650
Wire Wire Line
	4200 1650 10500 1650
Wire Wire Line
	4750 2500 4750 1750
Connection ~ 4750 1750
Wire Wire Line
	4750 1750 10500 1750
Wire Wire Line
	3950 5200 3950 1850
Connection ~ 3950 1850
Wire Wire Line
	3950 1850 4500 1850
Wire Wire Line
	4500 5200 4500 1850
Connection ~ 4500 1850
Wire Wire Line
	4500 1850 5050 1850
Wire Wire Line
	5050 5200 5050 1850
Connection ~ 5050 1850
Wire Wire Line
	5050 1850 10500 1850
Wire Wire Line
	750  5000 4050 5000
Connection ~ 5150 5000
Wire Wire Line
	5150 5000 10850 5000
Connection ~ 4600 5000
Wire Wire Line
	4600 5000 5150 5000
Connection ~ 4050 5000
Wire Wire Line
	4050 5000 4600 5000
Text Notes 500  950  0    50   ~ 0
/2
$EndSCHEMATC
